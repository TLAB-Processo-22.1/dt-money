import { Dashboard } from "./components/Dashboard";
import { Header } from "./components/Header";
import { GlobalStyle } from "./style/global";
import { useState } from "react";
import Modal from 'react-modal';
import { NewTransactionModal } from "./components/NewtransactionModal";
import { TransactionsContext, TransactionsProvider } from "./TransactionsContext";

Modal.setAppElement('#root');

export function App() {
  //criando modal
  const [isNewTransactionModalOpen, setIsNewTrasactionModal] = useState(false);
  
  function handleOpenNewTransactionModal(){
    setIsNewTrasactionModal(true)
  }

  function handleCloseNewTransactionModal(){
    setIsNewTrasactionModal(false)
  }
  return (
    <TransactionsProvider>
      <Header onOpenNewTransactionModal={handleOpenNewTransactionModal}/>
  
      <Dashboard/>

      <NewTransactionModal
        isOpen={isNewTransactionModalOpen}
        onRequestClose={handleCloseNewTransactionModal}
      />
      <GlobalStyle/>
    </TransactionsProvider>
  );
}

