import { Container } from "./styles";
import incomeImg from '../../assets/income.svg';
import expenseImg from '../../assets/expense.svg';
import totalImg from '../../assets/total.svg';
import { useContext } from "react";
import { TransactionsContext } from "../../TransactionsContext";

export function Summary(){
  //chamando use context
  const {transactions} = useContext(TransactionsContext);
  console.log(transactions)
  

  return(
    <>
      <Container>
        <div>
          <header>
            <p>Entradas</p>
            <img src={incomeImg} alt="Entradas"/>
          </header>
          <strong>R$1000,00</strong>
        </div>

        <div>
          <header>
            <p>Saídas</p>
            <img src={expenseImg} alt="Saídas"/>
          </header>
          <strong>R$-500,00</strong>
        </div>

        <div className="destaque-background">
          <header>
            <p>Total</p>
            <img src={totalImg} alt="total"/>
          </header>
          <strong>R$500,00</strong>
        </div>
      </Container>
    </>
  );
}