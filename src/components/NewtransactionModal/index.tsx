import Modal from 'react-modal';
import { Container, TransictionTypeContainer, RadioBox} from './styles';
import closeImg from '../../assets/minus.svg';
import entradaImg from '../../assets/income.svg';
import outputImg from '../../assets/expense.svg';
import { FormEvent, useState, useContext} from 'react';
import { api } from '../../services/api';
import { TransactionsContext } from '../../TransactionsContext';

interface NewTransactionModalProps{
  isOpen: boolean,
  onRequestClose: () => void;
}

export function NewTransactionModal({isOpen, onRequestClose}: NewTransactionModalProps) {
  //chamando contexto
  const {createTransaction} = useContext(TransactionsContext);
  
  //estado
  const [title, setTitle] = useState('');
  const [amount, setAmout] = useState(0);
  const [category, setCategory] = useState('');
  const [type, setType] = useState('deposit');

  //função que vem atraves da açaõ do usuario
  //
  function handleCreateNewTransaction(event: FormEvent) {
    event.preventDefault();
    createTransaction({
      title,
      amount,
      category,
      type
    })
  }

  return(
    <Modal 
    isOpen={isOpen}
    onRequestClose={onRequestClose}
    overlayClassName="react-modal-overlay"
    className="react-modal-content"
    >
      <button 
        type="button" 
        onClick={onRequestClose} 
        className="react-modal-close">
          <img src={closeImg} 
          alt="Fechar modal" />
      </button>
      <Container onSubmit={handleCreateNewTransaction}> 
          <h2>Cadastrar transação</h2>
          <input 
            placeholder="Título"
            value={title}
            onChange={event => setTitle(event.target.value)}
          />

          <input 
            type="number"
            placeholder="Valor"
            value={amount}
            onChange={event => setAmout(Number(event.target.value))}
          />
          
          <TransictionTypeContainer>
            <RadioBox
              type='button'
              onClick={() => {setType('deposit');}}
              isActive={type === 'deposit'}
              activeColor='green'
              >
              <img src={entradaImg}/>
              <span>Entrada</span>
          </RadioBox>

            <RadioBox
              type='button'
              onClick={() => {setType('withdraw');}}
              isActive={type === 'withdraw'}
              activeColor='red'
              >
              <img src={outputImg}/>
              <span>Saída</span>
            </RadioBox>

          </TransictionTypeContainer>

          <input 
            placeholder="Categoria"
            value={category}
            onChange={event => setCategory(event.target.value)}
          />

          <button type="submit">
            Cadastrar
          </button>
        </Container>
      </Modal>
  );
}