import { Container, Content } from "./styles";
import logoImg from '../../assets/logo.svg';

interface Headerprops{
  onOpenNewTransactionModal: () => void;
}

export function Header({onOpenNewTransactionModal}: Headerprops) {
    return(
    <>
      <Container>
        <Content>
          <img src={logoImg}/>
          <button type="button" onClick={onOpenNewTransactionModal}>
            Nova transação
          </button>
        </Content>
      </Container>
    </>
  );
}